# Ping Pong Leaderboard Code Challenge

## Requirements

Build a live leaderboard to display the current status of Anow's ping-pong
players based off of the web services provided by [this](https://bitbucket.org/appraisersnow/anow-leaderboard-api) project.

Have some fun and complete as much as you can. We understand if you don't meet
all of the requirements, but the basic requirements that need to be fulfilled
are as follows:

-   Build this leaderboard using the latest version of `React`
-   Use `Webpack`, `browserify`, `rollup.js` (or another similar tool) to bundle
    your application
-   Configure the project so that the following is all that is required for someone 
    cloning this repository to have it running locally right away (we tend to use 
    [yarn](https://yarnpkg.com) rather than [npm](https://www.npmjs.com/)):

```
yarn
yarn start
```

-   Consume the websocket or RESTful services provided by the [anow-leaderboard-services](https://bitbucket.org/appraisersnow/anow-leaderboard-api)
    project.
-   Implement the following user stories:
    -   I can display this web page on a monitor (say 19" - 30") on the wall in
        the office and it is readable from up to 3 meters away
    -   I can toggle the rating/sorting of users by raw 
        [Elo](https://en.wikipedia.org/wiki/Elo_rating_system) rating
        (returned by the API), or by the user's win/loss ratio (you'll need to
        calculate this). For the latter, in the event of a tie, the person with
        most wins is the leader.  For example, if Stephen has
        12 wins and 6 losses, and Dan has 6 wins and 3 losses, they both have a
        win/loss ratio of 2.  However, Stephen is considered the leader because
        he has more wins)
    -   The leaderboard shows at least the top 3 people.
    -   I can see at  person's name, Elo rating, wins, losses at a glance
        (i.e. I don't need to drill down to get these details.)
    -   I want the leader board to be pleasing to the eye (we are looking for 
        something that you consider polished enough to put in front of a paying 
        client as a final product).
    -   The leaderboard should update in near real-time when the scores change.
    -   If the backend service is down, the leaderboard should handle this
        gracefully with no intervention from a user.
    -   I should be able to view this leader board across the latest versions of
        Chrome, Firefox and Edge.

### Stretch requirements
Here are some additional things you can add if you wish to take this further:

-   Use `Less` or `Sass` (or another similar tool) to control your CSS
-   Use `Redux` if you deem it an appropriate tool for the type of project.
-   Add scripts to build and bundle your app.  We prefer projects to be 
    self-contained and rely on as few globally installed tools as possible.  
    We also use linters and other such tools to ensure our code is maintainable and 
    correct.
-   Write your code in the latest ES version, transpiling it to run in any modern browser.
-   Ensure that the application works in IE11 and the latest Safari.
-   Brand it as Anow (i.e. make it fit with anow.com and our application at secure.anow.com)
-   Abide by the coding standards the linters in this project enforce
-   Auto-scroll, ticker tape or some way of automatically showing the entire leaderboard
-   Add automated unit tests for your code.  You could also add code coverage
    and cyclomatic complexity checks.
-   Use `JSDoc` or similar to document your code
-   Configure the project to run inside Docker (preferably using `docker-compose up` 
    as the command to start it)
-   Ensure that all of your npm scripts are cross-platform
-   Anything else that you think would make this fun and help show your skills

## Project structure

You may use this project as a basis, though that is not required.  This project
may save you a little time, as the following [npm scripts](https://docs.npmjs.com/misc/scripts)
and tools are already configured:

-   `yarn lint` - runs the following linters:
    -   `eslint` - on all .js files not in `node_modules` or `bin`. It uses the
         airbnb rules.
    -   `remark` - on all .md files not in `node_modules` or `bin`. It uses the
         lint-recommended set of rules.
    -   `sass-lint` - on all .scss files not in `node_modules` or `bin`.
-   `yarn clean` - removes generated folders from the repo.  See below for
    more info on project structure.

As you noticed above, we use `yarn` rather than `npm` for dependency and package management.

The intended project structure is as follows (feel free to follow as much or as
little of it as you want).  *Italicized* items are considered
places for generated artifacts and should be removed by the `npm run clean`
command as well as not being checked into version control.

-   `/` - the project root
    -   `src` -  All source code should be organized under
        here (pick whatever structure feel natural to you).  This includes
        javascript, sass, css, html, etc.  You can also include unit tests here
        if you like (we usually use a `.spec.js` extension to signify and find
        them), or you can use the `test` folder below.
    -   `test` - All automated test code may reside here instead of `.spec.js`
        files in the main `src` folder.
    -   *`bin`* - This is where the output of any build steps should reside (e.g.
        Babel transpilation, Sass processing, etc.).
    -   *`doc`* - documentation such as output from documentation processing,
        unit test results, code coverage, etc. ends up here.
